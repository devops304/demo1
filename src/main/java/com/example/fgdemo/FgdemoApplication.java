package com.example.fgdemo;

import org.springframework.boot.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;


@RestController
@EnableAutoConfiguration
@SpringBootApplication
public class FgdemoApplication {

        @RequestMapping("/")
        String home() {
                return "Greetings from Spring Boot!";
        }
        public static void main(String[] args) {
                SpringApplication.run(FgdemoApplication.class, args);
        }

}
